
var AWS = require("aws-sdk")

exports.handler = (event, context, cb) => {
    
    AWS.config.update({
      region: "eu-central-1"
    });
    
    var docClient = new AWS.DynamoDB.DocumentClient();
    
    var year = 2015;
    const newId = Math.floor(Math.random()*9e9);
    var title = "The Big New Movie";
    
    var params = {
        TableName:"Items",
        Key:{
            "id":event.pathParameters.itemId,
        },
        ProjectionExpression: ['id','archetypeId','title']
    };
    
    docClient.get(params, function(err, data) {
        if (err) {
            
            console.error("Unable to get item. Error JSON:", JSON.stringify(err, null, 2));
            
            cb(err,{
                statusCode: 500,
                body: JSON.stringify({event,message:"error"}),
            })
            
        } else {
            
            docClient.get({TableName:"Archetypes",Key:{"id":data.Item.archetypeId},ProjectionExpression:["id","title"]},(err2,data2)=>{
                if(err2) {
                            
                    cb(err2,{
                        statusCode: 500,
                        body: JSON.stringify({event,message:"error2"}),
                    })
                    
                } else {
                    
                    cb(null,{
                        statusCode: 200,
                        body: JSON.stringify({data:{...data.Item,archetype:data2.Item},message:"item archetype was found"}),
                    })
                    
                }
            })
            
        }
        
    });
    
};
