
var AWS = require("aws-sdk")

exports.handler = (event, context, cb) => {
    
    AWS.config.update({
      region: "eu-central-1"
    });
    
    var docClient = new AWS.DynamoDB.DocumentClient();
    
    var year = 2015;
    const newId = Math.floor(Math.random()*9e9);
    
           let body =JSON.parse( event.body);
            
    var params = {
        TableName:"dankenorman_einsendungen",
        Item:{
            "id":"id_"+newId,
            "name": body.name,
            "phone": body.phone,
            "wetransfer": body.wetransfer,
            "notes": body.notes
        }
    };
    
    

    console.log("Adding a new item... with id "+ newId);
    console.error("This is test error message")
        
    docClient.put(params, function(err, data) {
        if (err) {
            console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
            cb(err,{
                statusCode: 500,
                body: JSON.stringify({event,message:"error"}),
            })
        } else {
            console.log("Added item:", JSON.stringify(params));
        
            cb(null,{
                statusCode: 200,
                body: JSON.stringify({event,message:"item was inserted"}),
            })
        }
    });
    
};
